$('#carouselExample').on('slide.bs.carousel', function (e) {

  
    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 4;
    var totalItems = $('.item-produk-jadi').length;
    
    if (idx >= totalItems-(itemsPerSlide-1)) {
        var it = itemsPerSlide - (totalItems - idx);
        for (var i=0; i<it; i++) {
            // append slides to end
            if (e.direction=="left") {
                $('.item-produk-jadi').eq(i).appendTo('.inner-produk-jadi');
            }
            else {
                $('.item-produk-jadi').eq(0).appendTo('.inner-produk-jadi');
            }
        }
    }
});


$('#carouselExample').carousel({ 
    interval: 2000
});

$('#carouselTestimony').on('slide.bs.carousel', function (e) {

  
    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 4;
    var totalItems = $('.item-testimony').length;
    
    if (idx >= totalItems-(itemsPerSlide-1)) {
        var it = itemsPerSlide - (totalItems - idx);
        for (var i=0; i<it; i++) {
            // append slides to end
            if (e.direction=="left") {
                $('.item-testimony').eq(i).appendTo('.inner-testimony');
            }
            else {
                $('.item-testimony').eq(0).appendTo('.inner-testimony');
            }
        }
    }
});


$('#carouselTestimony').carousel({ 
    interval: 2000
});


  $(document).ready(function() {
/* show lightbox when clicking a thumbnail */
    $('a.thumb').click(function(event){
      event.preventDefault();
      var content = $('.modal-body');
      content.empty();
        var title = $(this).attr("title");
        $('.modal-title').html(title);        
        content.html($(this).html());
        $(".modal-profile").modal({show:true});
    });

  });