<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Global_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->name_icentive = "dilokaproject_cmsicp";
		$this->name_business = "dilokaproject_business_care";
		$this->db_icentive = $this->load->database('icentive',TRUE);
		$this->db_business = $this->load->database('business',TRUE);
	}

	public function list_data()
	{
		$sql = $this->db->select('customer.*, sales.nama as nama_sales')
						->from('customer')
						->join('sales', 'sales.id=customer.id_sales','LEFT');
		return $sql->get();
	}

	public function get_global($table, $array = FALSE)
	{
		if($array) :

			$this->db->where($array);
			$query = $this->db->get($table);

		else :

			$query = $this->db->get($table);

		endif;

		return $query;
	}

	public function get_many_global($table, $array = FALSE)
	{
		if($array) :
	
			foreach($array as $row) {
				$this->db->where($row);
			}
			$query = $this->db->get($table);

		else :

			$query = $this->db->get($table);

		endif;

		return $query;
	}

	public function get_many_global_inc($table, $array = FALSE)
	{
		if($array) :
	
			foreach($array as $row) {
				$this->db_icentive->where($row);
			}
			$query = $this->db_icentive->get($table);

		else :

			$query = $this->db_icentive->get($table);

		endif;

		return $query;
	}

	public function get_global_incentive($table, $array = FALSE)
	{
		if($array) :

			$this->db_icentive->where($array);
			$query = $this->db_icentive->get($table);

		else :

			$query = $this->db_icentive->get($table);

		endif;

		return $query;
	}

	public function get_global_business($table, $array = FALSE)
	{
		if($array) :

			$this->db_business->where($array);
			$query = $this->db_business->get($table);

		else :

			$query = $this->db_business->get($table);

		endif;

		return $query;
	}

	public function count_global($table, $array=FALSE, $group_by=FALSE, $or_where=FALSE, $where=FALSE)
	{
		if($array) :

		$this->db_icentive->where($array);

		endif;

		if($or_where) :

		$this->db_icentive->or_where($or_where);

		endif;

		if($where) :

		$this->db_icentive->where($where);

		endif;

		if($group_by):

		foreach($group_by as $group_by){

			$this->db_icentive->group_by($group_by);

		}

		endif;

		$this->db_icentive->from($table);

		return $this->db_icentive->count_all_results();
	}

	public function count_global_business($table, $array=FALSE, $group_by=FALSE, $or_where=FALSE, $where=FALSE)
	{
		if($array) :

		$this->db_business->where($array);

		endif;

		if($or_where) :

		$this->db_business->or_where($or_where);

		endif;

		if($where) :

		$this->db_business->where($where);

		endif;

		if($group_by):

		foreach($group_by as $group_by){

			$this->db_business->group_by($group_by);

		}

		endif;

		$this->db_business->from($table);

		return $this->db_business->count_all_results();
	}

	public function add_global($table, $data)
	{

		$this->db->insert($table, $data);
		$query = $this->db->insert_id();
		return $query;
	}

	public function add_global_business($table, $data)
	{

		$this->db_business->insert($table, $data);
		$query = $this->db_business->insert_id();
		return $query;
	}

	public function add_global_inc($table, $data)
	{

		$this->db_icentive->insert($table, $data);
		$query = $this->db_icentive->insert_id();
		return $query;
	}

	public function add_batch($table, $data)
	{
	    if($data) {
	      	return $this->db->insert_batch($table, $data);
	    } else {
	      	return FALSE;
	    }
	}

	public function add_batch_inc($table, $data)
	{
	    if($data) {
	      	return $this->db_icentive->insert_batch($table, $data);
	    } else {
	      	return FALSE;
	    }
	}

	public function add_batch_bus($table, $data)
	{
	    if($data) {
	      	return $this->db_business->insert_batch($table, $data);
	    } else {
	      	return FALSE;
	    }
	}

	public function update_global($table, $data, $where)
	{
		$this->db->where($where);
		$query = $this->db->update($table, $data);
		return $query;
	}

	public function update_global_inc($table, $data, $where)
	{
		$this->db_icentive->where($where);
		$query = $this->db_icentive->update($table, $data);
		return $query;
	}

	public function update_batch($table, $data, $where)
	{
		$this->db->where($where);
		$query = $this->db->updateBatch($table, $data);
		return $query;
	}

	public function update_batch_inc($table, $data, $where)
	{
		$this->db_icentive->where($where);
		$query = $this->db_icentive->updateBatch($table, $data);
		return $query;
	}

	public function update_many_global($table, $data, $where)
	{
		foreach($where as $row) {
			$this->db->where($row);
		}
		$query = $this->db->update($table, $data);
		return $query;
	}

	public function delete_global($table, $where)
	{
		$this->db->where($where);
		$query = $this->db->delete($table);
		return $query;
	}

	public function delete_global_bus($table, $where)
	{
		$this->db_business->where($where);
		$query = $this->db_business->delete($table);
		return $query;
	}

	public function select_sum($table, $where=FALSE, $field, $like=FALSE, $group=FALSE)
	{
	    
	    $this->db->select_sum($field);
	    
	    if($where) :
	    
	      $this->db->where($where);

	    endif;
	    
	    if($like) :
	    
	      $this->db->like($like);

	    endif;
	    
	    if($group) :
	    
	      $this->db->group_by($group);

	    endif;
	    
	    $query = $this->db->get($table);

	    return $query;
	}

	public function select_sum_inc($table, $where=FALSE, $field, $like=FALSE, $group=FALSE)
	{
	    
	    $this->db_icentive->select_sum($field);
	    
	    if($where) :
	    
	      $this->db_icentive->where($where);

	    endif;
	    
	    if($like) :
	    
	      $this->db_icentive->like($like);

	    endif;
	    
	    if($group) :
	    
	      $this->db_icentive->group_by($group);

	    endif;
	    
	    $query = $this->db_icentive->get($table);

	    return $query;
	}

	public function contactus_kode()   {

		  $this->db->select('RIGHT(kode,10) as kode', FALSE);
		  $this->db->order_by('id','DESC');
		  $this->db->limit(1);
		  $this->db->like('kode', "CS".date('ym'));
		  $query = $this->db->get('contactus');
		  if($query->num_rows() <> 0){
		   $data = $query->row();
		   $kode = intval($data->kode) + 1;
		  } else {
		   $kode = 1;
		  }

		  $kodemax = str_pad(substr($kode,-6), 6, "0", STR_PAD_LEFT); // angka 4 menunjukkan jumlah digit angka 0
		  $kodejadi = "CS".date('ym').$kodemax;    // hasilnya ODJ-9921-0001 dst.
		  return $kodejadi;  

	}

	public function artikel_kode()   {

		  $this->db->select('RIGHT(kode,14) as kode', FALSE);
		  $this->db->order_by('id_blog','DESC');
		  $this->db->limit(1);
		  $this->db->like('kode', "YS".$this->session->userdata('userid').date('ym'));
		  $query = $this->db->get('blog');
		  if($query->num_rows() <> 0){
		   $data = $query->row();
		   $kode = intval($data->kode) + 1;
		  } else {
		   $kode = 1;
		  }

		  $kodemax = str_pad(substr($kode,-4), 4, "0", STR_PAD_LEFT); // angka 4 menunjukkan jumlah digit angka 0
		  $kodejadi = "YS".$this->session->userdata('userid').date('ym').$kodemax;    // hasilnya ODJ-9921-0001 dst.
		  return $kodejadi;  
	}

	public function subscribe_kode()   {

		  $this->db->select('RIGHT(kode,10) as kode', FALSE);
		  $this->db->order_by('sid','DESC');
		  $this->db->limit(1);
		  $this->db->like('kode', "SB".date('ym'));
		  $query = $this->db->get('subscriber');
		  if($query->num_rows() <> 0){
		   $data = $query->row();
		   $kode = intval($data->kode) + 1;
		  } else {
		   $kode = 1;
		  }

		  $kodemax = str_pad(substr($kode,-6), 6, "0", STR_PAD_LEFT); // angka 4 menunjukkan jumlah digit angka 0
		  $kodejadi = "SB".date('ym').$kodemax;    // hasilnya ODJ-9921-0001 dst.
		  return $kodejadi;  

	}

	public function notifikasi_list($name=FALSE, $limit=FALSE, $offset=FALSE, $status=FALSE)
	{
		$this->db->select('*');
		$this->db->from('log_notifikasi');
		$this->db->where('userid', $this->userid);
		
		if($name) :
			$this->db->where('kategori', $name);
		endif;
		
		if($status) :
			$this->db->where('status', $status);
		endif;

		if($limit) :
			$this->db->limit($limit, $offset);
		endif;
		
		$this->db->order_by('id_notifikasi', 'DESC');

		$query = $this->db->get();
		return $query;
	}

	public function blog_kategori()
	{
		$this->db->select('*');
		$this->db->from('blog_kategori');
		$this->db->where('status', 'ON');
		$this->db->order_by('urutan', 'ASC');
		$query = $this->db->get();
		return $query;
	}

	public function artikel_terkini($limit=FALSE, $offset=FALSE)
	{
		$this->db->select('blog.*, blog_kategori.*');
		$this->db->from('blog');
		$this->db->join('blog_kategori', 'blog_kategori.id_blog_kategori = blog.kategori', 'left');

		$this->db->where('blog.status =', 'ON');
		$this->db->where('blog.approved =', 1);
		$this->db->where('blog.approve_public =', 1);
		if ($limit == 4 && $offset == 1) {
			$this->db->where('blog.kategori !=', 5);
		}
		$this->db->order_by('tanggal_publish', 'desc');		
		if($limit) :
			$this->db->limit($limit, $offset);
		endif;


		$query = $this->db->get();
		return $query->result_array();
	}	

	public function artikel_highlight()
	{
		$this->db->select('blog.*, blog_kategori.*');
		$this->db->from('blog');
		$this->db->join('blog_kategori', 'blog_kategori.id_blog_kategori = blog.kategori', 'left');

		$this->db->where('blog.status =', 'ON');
		$this->db->where('blog.approved =', 1);
		$this->db->where('blog.kategori !=', 5);
		$this->db->order_by('tanggal_publish', 'desc');		
		$this->db->limit('1');


		$query = $this->db->get();
		return $query->row();
	}	

	public function yourstories_highlight()
	{
		$this->db->select('blog.*, blog_kategori.*');
		$this->db->from('blog');
		$this->db->join('blog_kategori', 'blog_kategori.id_blog_kategori = blog.kategori', 'left');

		$this->db->where('blog.status =', 'ON');
		$this->db->where('blog.approve_public =', 1);
		$this->db->where('blog.highlight_public =', 1);
		$this->db->where('blog.kategori =', 5);
		$this->db->order_by('tanggal_publish', 'desc');		
		$this->db->limit('1');


		$query = $this->db->get();
		return $query->row();
	}

	public function artikel_kategori($name=FALSE,$limit=FALSE, $offset=FALSE)
	{
		$this->db->select('blog.*, blog_kategori.*');
		$this->db->from('blog');
		$this->db->join('blog_kategori', 'blog_kategori.id_blog_kategori = blog.kategori', 'left');

		$this->db->where('blog.status =', 'ON');
		$this->db->where('blog.approve_public =', 1);
		// $this->db->where('blog.approved =', 1);
		
		if($name){
			$name = str_replace('-', ' ', ucwords($name));
			$this->db->where('blog_kategori.nama =', $name);
		}

		if($limit) :
			$this->db->limit($limit, $offset);
		endif;

		$query = $this->db->get();
		return $query;
	}

	public function artikel_rekomended()
	{
		$this->db->select('blog.*, blog_kategori.*');
		$this->db->from('blog');
		$this->db->join('blog_kategori', 'blog_kategori.id_blog_kategori = blog.kategori', 'left');

		$this->db->where('blog.status =', 'ON');
		$this->db->where('blog.approved =', 1);
		$this->db->limit(4);

		$query = $this->db->get();
		return $query->result_array();
	}

	public function artikel_random()
	{
		$this->db->select('blog.*, blog_kategori.*');
		$this->db->from('blog');
		$this->db->join('blog_kategori', 'blog_kategori.id_blog_kategori = blog.kategori', 'left');

		$this->db->where('blog.status =', 'ON');
		$this->db->where('blog.approved =', 1);
		$this->db->order_by('rand()', 'DESC');
		$this->db->limit(4);

		$query = $this->db->get();
		return $query->result_array();
	}

	public function artikel_detail($id_artikel=FALSE, $limit=FALSE, $offset=FALSE)
	{
		$this->db->select('blog.*, blog_kategori.*');
		$this->db->from('blog');
		$this->db->join('blog_kategori', 'blog_kategori.id_blog_kategori = blog.kategori', 'left');

		$this->db->where('blog.status =', 'ON');

		if($id_artikel) :
			$this->db->where('blog.id_blog', $id_artikel);
		endif;

		if($limit) :
			$this->db->limit($limit, $offset);
		endif;

		$query = $this->db->get();
		return $query;
	}

	public function get_artikel_detail($where = false)
	{
		$this->db->select('blog.*, blog_kategori.*');
		$this->db->from('blog');
		$this->db->join('blog_kategori', 'blog_kategori.id_blog_kategori = blog.kategori', 'left');

		$this->db->where('blog.status =', 'ON');

		if($where) :
			$this->db->where($where);
		endif;

		$query = $this->db->get();
		return $query;
	}

    function getnewprovinsi()
    {
        $this->db->from('tbl_provinsi');
        $this->db->group_by('id');
        $this->db->order_by("provinsi", "ASC");
        return $this->db->get();
    }

    function getnewKota($id)
    {
        $this->db->from('tbl_kabkot');
        $this->db->where('provinsi_id', $id);
        $this->db->group_by('kabupaten_kota');
        return $this->db->get();
    }

    function getnewkecamatan($id)
    {
        $this->db->from('tbl_kecamatan');
        $this->db->where('kabkot_id', $id);
        $this->db->group_by('kecamatan');
        return $this->db->get();
    }

    function getnewkelurahan($id)
    {
        $this->db->from('tbl_kelurahan');
        $this->db->where('kecamatan_id', $id);
        $this->db->group_by('kelurahan');
        return $this->db->get();
    }

    function getnewkelurahan2($id)
    {
        $this->db->from('tbl_kelurahan');
        $this->db->where('id', $id);
        $this->db->group_by('kd_pos');
        return $this->db->get();
    }

    public function pencarian($cari){
		// alter table blog add fulltext(judul);
		// alter table produk add fulltext(nama_produk);
		// alter table produk_paket add fulltext(nama_paket);
		$this->db->select('blog.*, blog_kategori.nama AS nama');
		$this->db->from('blog');
		$this->db->join('blog_kategori', 'blog_kategori.id_blog_kategori = blog.kategori', 'left');
		$this->db->where("(blog.judul LIKE '%".$cari."%')");
		// $this->db->where('MATCH (judul) against ("'.$cari.'" IN BOOLEAN MODE)');
		$this->db->where('blog.status =', 'ON');

		$query = $this->db->get();
		return $query;
    }

    public function pencarian_produk($cari)
    {
    	$this->db_icentive->select('produk.*, kategori_deskripsi.id_kategori, kategori_deskripsi.nama_kategori, kategori_deskripsi.deskripsi, kategori.*');
		$this->db_icentive->from('produk');
		$this->db_icentive->join('kategori_deskripsi', 'kategori_deskripsi.id_kategori = produk.master_kategori_id');
		$this->db_icentive->join('kategori', ' kategori.id_kategori = kategori_deskripsi.id_kategori');
		$this->db_icentive->join('master_harga', ' master_harga.id_product = produk.id');
		$this->db_icentive->where("(produk.nama_produk LIKE '%".$cari."%')");
		// $this->db_icentive->where('MATCH (nama_produk) against ("'.$cari.'" IN BOOLEAN MODE)');

		$where = array('status_produk' => 'Active', 'master_harga.pv !=' => 0, 'master_harga.bv !=' => 0, 'master_harga.mp !=' => 0,'master_harga.cp !=' => 0);
		$this->db_icentive->where($where);

		$this->db_icentive->group_by('produk.kode_produk');

		$query = $this->db_icentive->get();
		return $query;
    }

    public function pencarian_produk_promo($cari){
		$this->db_icentive->select('*');
		$this->db_icentive->from('produk_paket');
		$this->db_icentive->where('produk_paket.status =', 'ON');
		$this->db_icentive->where('produk_paket.approved =', '1');
		$this->db_icentive->where('current_date() >= produk_paket.start_date');
		$this->db_icentive->where('current_date() <= produk_paket.end_date');
		
		$this->db_icentive->where("(produk_paket.nama_paket LIKE '%".$cari."%')");
		// $this->db_icentive->where('MATCH (nama_paket) against ("'.$cari.'" IN BOOLEAN MODE)');

		$query = $this->db_icentive->get();
		return $query;
	}

	public function cek_keanggotaan_member($userid){
		$this->db_icentive->select('*');
		$this->db_icentive->from('log_list_member');
		$this->db_icentive->where('no_member', $userid);
		$this->db_icentive->group_by('no_member');
		$query = $this->db_icentive->get();
		return $query;
	}

	function cek_log_loscked($userid,$activitas,$tanggal){
		$this->db->select('*');
		$this->db->from('log_locked');
		$this->db->where('member_id', $userid);
		$this->db->where('activitas', $activitas);
		$this->db->where("(tanggal LIKE '%".$tanggal."%')");
		// $this->db->where('tanggal <=', $tanggal);
		return $this->db->get();
		
	}

	function cek_suspend_locked($userid,$tanggal,$activitas){
		$this->db->select('*');
		$this->db->from('log_locked');
		$this->db->where('member_id', $userid);
		$this->db->where("(tanggal LIKE '%".$tanggal."%')");
		$this->db->where('activitas', $activitas);
		$this->db->where('status =', '1');
		return $this->db->get();
		
	}

	function cek_register_locked($email,$hp,$tanggal,$activitas){
		$this->db->select('*');
		$this->db->from('log_locked');
		$this->db->join($this->name_icentive.'.member_register', $this->name_icentive.'.member_register.userid = log_locked.member_id');
		$this->db->where($this->name_icentive.'.member_register.email', $email);
		$this->db->where($this->name_icentive.'.member_register.hp', $hp);
		$this->db->where('log_locked.activitas', $activitas);
		$this->db->where("(log_locked.tanggal LIKE '%".$tanggal."%')");
		$this->db->where('log_locked.status =', '1');
		return $this->db->get();
		
	}
}
