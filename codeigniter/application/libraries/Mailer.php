<?php defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Mailer {
    protected $_ci;

    public function __construct(){
        $this->_ci = &get_instance(); // Set variabel _ci dengan Fungsi2-fungsi dari Codeigniter

        $this->_ci->config->load('myconfig', TRUE);
        // $this->email_host      = $this->_ci->config->item('email_host');
        // $this->email_pengirim  = $this->_ci->config->item('email_email');
        // $this->password        = $this->_ci->config->item('email_pass');
        // $this->nama_pengirim   = $this->_ci->config->item('email_name');

        $this->_ci->load->model('global_model');
        $info_sender = $this->_ci->global_model->get_global_incentive('setting_sender_email', array('kode' => 'MULTICARE'))->row();
        $this->email_host       = $info_sender->host;
        $this->email_pengirim   = $info_sender->username;
        $this->password         = decode($info_sender->password);
        $this->nama_pengirim    = $info_sender->sender_name;
        $this->port             = $info_sender->port;
        $this->smtpauth         = $info_sender->smtpauth;
        $this->smtpsecure       = $info_sender->smtpsecure;
        $this->ishtml           = $info_sender->ishtml;

        require_once(APPPATH.'third_party/phpmailer/Exception.php');
        require_once(APPPATH.'third_party/phpmailer/PHPMailer.php');
        require_once(APPPATH.'third_party/phpmailer/SMTP.php');
    }

    public function send($data){
		try{
        $mail = new PHPMailer;
        $mail->isSMTP();

        $mail->Host = $this->email_host;
        $mail->Username = $this->email_pengirim; // Email Pengirim
        $mail->Password = $this->password; // Isikan dengan Password email pengirim
        $mail->Port = $this->port; //465;
        $mail->SMTPAuth = $this->smtpauth; //true;
        $mail->SMTPSecure = $this->smtpsecure; //'ssl';
        //$mail->SMTPDebug = 2; // Aktifkan untuk melakukan debugging

        $mail->setFrom($this->nama_pengirim);
        $mail->addAddress($data['email_penerima'], '');
        //$mail->addCC(@$data['email_cc']);
        $mail->isHTML($this->ishtml); // Aktifkan jika isi emailnya berupa html
        // $mail->isHTML(true);

        $mail->Subject = $data['subjek'];
        $mail->Body = $data['content'];
        // $mail->AddEmbeddedImage('image/logo.png', 'logo_mynotescode', 'logo.png'); // Aktifkan jika ingin menampilkan gambar dalam email

        $send = $mail->send();
		
        if($send){ // Jika Email berhasil dikirim
            $response = array('status'=>'Sukses', 'message'=>'Email berhasil dikirim');
        }else{ // Jika Email Gagal dikirim
            // $response =  $mail->ErrorInfo;
            $response = array('status'=>'Gagal', 'message'=>'Email gagal dikirim');
        }
		} catch (phpmailerException $e) {
			error_log( "An error occurred. {$e->errorMessage()}"); //Catch errors from PHPMailer.
		} catch (Exception $e) {
			error_log("Email not sent. {$mail->ErrorInfo}"); //Catch errors from Amazon SES.
		}
		
		//die();
        return $response;
    }

    public function send_with_attachment($data){
        $mail = new PHPMailer;
        $mail->isSMTP();

        $mail->Host = 'smtp.gmail.com';
        $mail->Username = $this->email_pengirim; // Email Pengirim
        $mail->Password = $this->password; // Isikan dengan Password email pengirim
        $mail->Port = 465;
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = 'ssl';
        // $mail->SMTPDebug = 2; // Aktifkan untuk melakukan debugging

        $mail->setFrom($this->email_pengirim, $this->nama_pengirim);
        $mail->addAddress($data['email_penerima'], '');
        $mail->isHTML(true); // Aktifkan jika isi emailnya berupa html

        $mail->Subject = $data['subjek'];
        $mail->Body = $data['content'];
        $mail->AddEmbeddedImage('image/logo.png', 'logo_mynotescode', 'logo.png'); // Aktifkan jika ingin menampilkan gambar dalam email

        if($data['attachment']['size'] <= 25000000){ // Jika ukuran file <= 25 MB (25.000.000 bytes)
            $mail->addAttachment($data['attachment']['tmp_name'], $data['attachment']['name']);

            $send = $mail->send();

            if($send){ // Jika Email berhasil dikirim
                $response = array('status'=>'Sukses', 'message'=>'Email berhasil dikirim');
            }else{ // Jika Email Gagal dikirim
                $response = array('status'=>'Gagal', 'message'=>'Email gagal dikirim');
            }
        }else{ // Jika Ukuran file lebih dari 25 MB
            $response = array('status'=>'Gagal', 'message'=>'Ukuran file attachment maksimal 25 MB');
        }

        return $response;
    }
}
