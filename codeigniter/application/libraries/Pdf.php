<?php
#------------------------------
#dateCreated " 8 Maret 2010 s/d 19 Maret 2012"
#createdBy : Ibnuyoga.
#ibnuyoga@gmail.com
#------------------------------
class Pdf
{
    protected $CI;

    /**
     * [__construct description]
     */
    function __construct()
    {
        //$this->CI =&get_instance();
        //$this->CI->load->library(array('email', 'ftp','user_agent','pagination'));
    }

    public function print_pdf($output,$name='Report',$paper="A4")
    {
        //ini_set('memory_limit','32M');
        if (ob_get_contents()){ ob_end_clean();}
        if (ob_get_length()) {ob_end_clean();}
        $CI =& get_instance();
        $CI->load->library('Mpdf6/Mpdf');
        $mpdf=new mPDF('utf-8', $paper, 0, '', 15, 15, 20, 20, 10, 10);
        $mpdf->debug = false;
        $mpdf->allow_output_buffering = true;
        $mpdf->cacheTables            = true;
        $mpdf->simpleTables           = true;
        $mpdf->packTableData          = true;
        $mpdf->SetTitle(ucwords(replace_underscore($name)));
        $mpdf->WriteHTML(file_get_contents(base_url('assets/css/print_pdf.css')),1);
        $mpdf->SetHeader('Elephate'.'||'.$name);
        $mpdf->SetFooter('Elephate'.'|{PAGENO}|'.date('Y-m-d')); // Add a footer for good measure ;)
        $mpdf->WriteHTML($output,2);
        $mpdf->Output($name,'I');
    }
}
?>