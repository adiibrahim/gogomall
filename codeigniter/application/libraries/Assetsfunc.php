<?php
#------------------------------
#dateCreated " 8 Maret 2010 s/d 19 Maret 2012"
#createdBy : Ibnuyoga.
#ibnuyoga@gmail.com
#------------------------------
class Assetsfunc
{
    protected $CI;

    /**
     * [__construct description]
     */
    function __construct()
    {
        $this->CI =&get_instance();
        $this->CI->load->library(array('email', 'ftp','user_agent','pagination'));
    }

        public function halaman($url=FALSE, $urlsegment= FALSE, $perpage=FALSE, $totalrows=FALSE)
    {
        $config                     = array();
        $config['base_url']         = base_url().$url;
        $config['total_rows']       = $totalrows;
        $config['num_links']        = 5;
        $config['per_page']         = $perpage;
        $config['uri_segment']      = $urlsegment;
        $config['use_page_numbers'] = FALSE;


        $config['full_tag_open'] = '<div class="pagination1"><ul>';
        $config['full_tag_close'] = '</ul></div><!--pagination-->';
        $config['first_link'] = '&laquo; First';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_link'] = 'Last &raquo;';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['next_link'] = 'Next &rarr;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '&larr; Previous';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="is-active"><a href="">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        // $config['display_pages'] = FALSE;
        //
        $config['anchor_class'] = 'follow_link';
                $this->CI->pagination->initialize($config);
                $halaman = $this->CI->pagination->create_links();
                return $halaman;
    }

    // public function halaman($url=FALSE,$urlsegment=FALSE,$perpage=FALSE,$totalrows=FALSE)
    // {
    //     $config                     = array();
    //     $config['base_url']         = base_url().$url;
    //     $config['total_rows']       = $totalrows;
    //     $config['num_links']        = 5;
    //     $config['per_page']         = $perpage;
    //     $config['uri_segment']      = $urlsegment;
    //     $config['use_page_numbers'] = FALSE;

    //     $config['full_tag_open']    = "<ul class='pagination'>";
    //     $config['full_tag_close']   ="</ul>";

    //     $config['first_link']       = "<span class='glyphicon glyphicon-fast-backward'></span>";
    //     $config['first_tag_open']   = "<li>";
    //     $config['first_tag_close']  = "</li>";

    //     $config['last_link']        = "<span class='glyphicon glyphicon-fast-forward'></span>";
    //     $config['last_tag_open']    = "<li>";
    //     $config['last_tag_close']   = "</li>";

    //     $config['next_link']        = "<span class='glyphicon glyphicon-forward'></span>";
    //     $config['next_tag_open']    = "<li>";
    //     $config['next_tag_close']   = "</li>";

    //     $config['prev_link']        = "<span class='glyphicon glyphicon-backward'></span>";
    //     $config['prev_tag_open']    = "<li>";
    //     $config['prev_tag_close']   = "</li>";

    //     $config['cur_tag_open']     = "<li class='active'><a href='#'>";
    //     $config['cur_tag_close']    = "<span class='sr-only'></span></a></li>";

    //     $config['num_tag_open']     = "<li>";
    //     $config['num_tag_close']    = "</li>";

    //     $this->CI->pagination->initialize($config);
    //     $halaman = $this->CI->pagination->create_links();
    //     return $halaman;
    // }
    
    public function checkIsAjax()
    {
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
        {
            //$this->isAjax = true;
            return true;
        }
        else
        {
            //$this->isAjax = false;
            return false;
        }
    }

    /**
     * [changeQuotes description]
     * @param  [type] $text [description]
     * @return [type]       [description]
     */
    public function changeQuotes($text)
    {
        $text = preg_replace("/'/", '&rsquo;', $text);
        return $text;
    }

    /**
     * [encode description]
     * @param  [type] $string [description]
     * @return [type]         [description]
     */
    public function urlencode($string)
    {
        $data = base64_encode($string);
        $data = str_replace(array('+','/','='),array('-','_',''),$data);
        return trim($data);
    }

    /**
     * [decode description]
     * @param  [type] $string [description]
     * @return [type]         [description]
     */
    public function urldecode($string)
    {
        $data = str_replace(array('-','_'),array('+','/'),$string);
        $mod4 = strlen($data) % 4;

        if ($mod4)
        {
            $data .= substr('====', $mod4);
        }

        return trim(base64_decode($data));
    }

    public function leftword($text,$length=64,$tail="...")
    {
        $text = trim($text);
        $txtl = strlen($text);

        if($txtl > $length)
        {
            for($i=1;$text[$length-$i]!=" ";$i++)
            {

                if($i == $length)
                {
                    return substr($text,0,$length) . $tail;
                }

            }

            $text = substr($text,0,$length-$i+1) . $tail;
        }


        if(substr($text, -1, 1) == "-")
        {
            $text = substr($text,0,strlen($text)-1) . $tail;
        }

        return $text;
    }

}

    // end class
?>
