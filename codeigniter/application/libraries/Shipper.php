<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shipper {

    public static $isProduction = true;

    public static $client_key;

    const PRODUCTION_URL = 'https://api.shipper.id/prod/public/v1/';
    const SANDBOX_URL = 'https://api.sandbox.shipper.id/public/v1/';

    public function config($params)
    {
        Shipper::$client_key = $params['client_key'];
        Shipper::$isProduction = $params['production'];
    }

        public static function getBaseUrl()
    {
        return Shipper::$isProduction ?
            Shipper::PRODUCTION_URL : Shipper::SANDBOX_URL;
    }

        public static function post($url, $client_key, $params)
    {
        return self::remoteCallPost($url, $client_key, $params);
    }

            public static function get($url, $client_key, $params)
    {
        return self::remoteCallGet($url, $client_key, $params);
    }

        public static function remoteCallPost($url, $client_key, $params)
    {

        $content = json_encode($params['data_create_order']);

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array(
            "Content-type: application/json",
            "User-Agent: ".$params['user_agent'],
            "clientkey: ".$client_key,
        ));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

        $json_response = curl_exec($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        return json_decode($json_response);

    }

            public static function remoteCallGet($url, $client_key, $params)
    {

        if($url == Shipper::getBaseUrl() . 'domesticRates') :
            $url_end = $url.'?'.$params['url'].'&apiKey='.$client_key;
        elseif($url == Shipper::getBaseUrl() . 'orders') :
            $url_end = $url.'/'.$params['shipping_id'].'?apiKey='.$client_key;
        else:
            $url_end = $url.'/'.$params['kodepos'].'?apiKey='.$client_key;
        endif;


        $curl = curl_init($url_end);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array(
            "User-Agent: ".$_SERVER['HTTP_USER_AGENT'],
        ));
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

        $json_response = curl_exec($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        $result = json_decode($json_response);

//         return $result;

// die;
        if($url == Shipper::getBaseUrl() . 'domesticRates') :

            return $result->data->rates->logistic;

        elseif($url == Shipper::getBaseUrl() . 'orders') :

            if($result->status === "success") :
                return $result->data->order;
            endif;

        else:
// die;
            $data = $result->data->rows;

            $nama_areas = $params['kelurahan'];

            $areas = '';

            foreach($data as $data_areas):
                $aa = like_match('%'.$nama_areas.'%', $data_areas->area_name);
                if($aa == true) :
                    $areas = $data_areas->area_id;
                // else:
                //     $areas = @$data_areas->area_id;
                //     if($areas) :
                //         $areas = $areas;
                //     else:
                //         $areas = null;
                //     endif;
                endif;
            endforeach;

            return $areas;

        endif;


    }


  public static function area_id($params)
  {

    $result = Shipper::get(
        Shipper::getBaseUrl() . 'details',
        Shipper::$client_key,
        $params
       );

    return $result;
  }


      public static function order_detail($params)
  {

    $result = Shipper::get(
        Shipper::getBaseUrl() . 'orders',
        Shipper::$client_key,
        $params
       );

    return $result;
  }

    public static function rates($params)
  {

    $result = Shipper::get(
        Shipper::getBaseUrl() . 'domesticRates',
        Shipper::$client_key,
        $params
       );

    return $result;

  }


  
  public static function order($params)
  {

    $result = Shipper::post(
        Shipper::getBaseUrl() . 'orders/domestics?apiKey='.Shipper::$client_key,
        Shipper::$client_key,
        $params
       );

    return $result;

  }

}
