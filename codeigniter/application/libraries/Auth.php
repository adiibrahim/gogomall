<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth {

    public static $isProduction = false;

    public static $client_key;

    const PRODUCTION_URL = '';
    const SANDBOX_URL = 'https://2fauth-m.smartyshop.co.id/';

    public function config($params)
    {
        Auth::$client_key = $params['client_key'];
        Auth::$isProduction = $params['production'];
    }

        public static function getBaseUrl()
    {
        return Auth::$isProduction ?
            Auth::PRODUCTION_URL : Auth::SANDBOX_URL;
    }

            public static function post($url)
    {
        return self::remoteCall($url);
    }

        public static function remoteCall($url)
    {   
            
        // $content = json_encode($params);

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET"); 
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array(
            "Content-type: application/json",
            'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiTXVsdGljYXJlJiYyMCIsImlhdCI6MTYwMDA2NjE2NX0.bNN74pAtwlV7vOAlZNn1N2oUSDa_922QbOlyRxSTTAY',
        ));
    
        curl_setopt($curl, CURLOPT_POST, true);
        // curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

        $json_response = curl_exec($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        return json_decode($json_response);

    }


  public static function generate()
  {
    
    $result = Auth::post(
        Auth::getBaseUrl() . '/generate'
    );

    return $result;
  }

  public static function verify($params)
  {
    
    $result = Auth::post(
        Auth::getBaseUrl() . '/verify/'.$params
    );

    return $result;
  }

}