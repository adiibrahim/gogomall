<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api {
    protected $_ci;

    public function __construct(){
        //config sms
        $this->_ci = &get_instance(); // Set variabel _ci dengan Fungsi2-fungsi dari Codeigniter
        $this->_ci->config->load('myconfig', TRUE);
        $this->apikey       = $this->_ci->config->item('apikey');
        $this->clientid     = $this->_ci->config->item('clientid');
        $this->senderid     = $this->_ci->config->item('senderid');

        //sms old
        // $this->link_send_otp        = $this->_ci->config->item('link_send_otp');
        // $this->link_cek_saldo_sms   = $this->_ci->config->item('link_cek_saldo_sms');

        require_once(APPPATH.'third_party/phpmailer/Exception.php');
        require_once(APPPATH.'third_party/phpmailer/PHPMailer.php');
        require_once(APPPATH.'third_party/phpmailer/SMTP.php');
    }

    public function insert_log_invoice($data)
    {
        extract($data);
        $link   = "https://enginepentest.elephate.co.id/engine/insert_log_invoice";
        $key    = encode("Multicare".$no_invoice.$status_proses);

        $curl   = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $link,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => array('no_invoice' => $no_invoice,'created_by' => $created_by,'status_proses' => $status_proses,'keterangan' => $keterangan,'key' => $key),
          CURLOPT_HTTPHEADER => array(
            'Cookie: ci_session=ie976dmln8n7nbok69h0gvhovupnp7tt'
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        // echo $response;
    }

    public function member_network($data)
    {
      extract($data);
      $link   = "https://enginepentest.elephate.co.id/engine/member_network";
      $key    = encode('Multicare'.$userid.$referral_id);

      $curl   = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => $link,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => array('userid' => $userid,'referral_id' => $referral_id,'key' => $key),
        CURLOPT_HTTPHEADER => array(
          'Cookie: ci_session=tj13if5gmssbg7ik0sv1qkkmgueb89t8'
        ),
      ));

      $response = curl_exec($curl);

      curl_close($curl);
      // echo $response;
    }

    public function cek_stok_produk($id_produk, $status = false)
    {
      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://enginepentest.elephate.co.id/engine/running_stock/'.$id_produk.'/'.$status,
        CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
          'Cookie: ci_session=d6nerp6rqi8416lbb13d6fgan4jq6kbk'
        ),
      ));
      $response = curl_exec($curl);
      $data = json_decode($response);
      curl_close($curl);
      if ($data == null) {
        return 0;
      }else{
        return @$data->sisa;
      }
    }

    public function cek_saldo_sms()
    {
      $link = "https://apidev.tcastsms.net/api/v2/Balance?ApiKey=".$this->apikey."&ClientId=".$this->clientid;

      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => $link,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
          'Cookie: PHPSESSID=c9a30f1f5ff32da98355af4386b54d89'
        ),
      ));

      $response = curl_exec($curl);

      curl_close($curl);
      $json  = json_encode($response);
      $jsond  = json_decode($response,TRUE);
      foreach ($jsond as $key => $value) {
        $idr =  $value[0]['Credits'];
      }

      if ($idr) {
        $saldo = explode('IDR', $idr);
        return $saldo[1];
      }else{
        return 0;
      }
    }

    public function sendsms($phone, $message)
    {
      // $link = $this->link_send_otp.'&numbers='.$phone.'&content='.$message;

      $link = "https://apidev.tcastsms.net/api/v2/SendSMS?ApiKey=".$this->apikey."&ClientId=".$this->clientid."&SenderId=".$this->senderid."&Message=".$message."&MobileNumbers=".$phone."&Is_Unicode=false&Is_Flash=false";

      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => $link,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
          'Cookie: PHPSESSID=c9a30f1f5ff32da98355af4386b54d89'
        ),
      ));

      $response = curl_exec($curl);

      curl_close($curl);
    }

    public function upload($method, $url, $data)
    {
      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => $method,
        CURLOPT_POSTFIELDS => $data,
        CURLOPT_HTTPHEADER => array(
          'Cookie: sess_admin_academy=tokn18d6botvudtptj827gmprrkbbelt'
        ),
      ));

      $response = curl_exec($curl);
      curl_close($curl);
      return $response;
    }

    public function get_from_elephates()
    {
      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://graph.instagram.com/me/media?fields=id,media_url,media_type,permalink,thumbnail_url&access_token=IGQVJXRmRRRkxRdkZAWUTl1RV91WWlIZAFdTdk9BeGVuSG1yczZAaTlhzUmxCUV9GOC1GSjV1WkxqX05uRFZAMazFsbHp6TXd1d3ZAGWjFrRk1UZAGpfMVozUVhVZAnI2Y1F5eE5DbUpiNDNxUkc3Vndmei10SQZDZD',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
          'Cookie: ig_did=BB2BA670-A835-4C78-88C8-5C264D869A2D; mid=X5hpgQAEAAH1QNlOuZoURYeUv47y; ig_nrcb=1; csrftoken=yOScfNgoS4jZU65hpB8zZm5l1VXbYvpm'
        ),
      ));

      $response = curl_exec($curl);

      curl_close($curl);

      $res = json_decode($response);
      return $res;
    }

	function kirim($MobileNumbers,$Message)
	{
		$SenderId = 'ELEPHATE';
		$ApiKey = 'e/sYa4t7gITLGDq2LHIjm8dHpTLEdh2Cni8it++Qcn4=';
		$ClientId = '158fa32d-80c6-4ab4-89a5-1870a5b1cd49';
		//$Message = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus dictum ut tortor eget lacinia';
		//$MobileNumbers =  '6281298796746';
		$Is_Unicode =  'false';
		$Is_Flash =  'false';
		$url     = "https://apidev.tcastsms.net/api/v2/SendSMS?";
		$content = array(
			'ApiKey' => $ApiKey,
			'ClientId' => $ClientId,
			'SenderId' => $SenderId,
			'Message' => $Message,
			'MobileNumbers' => $MobileNumbers,
			'Is_Unicode' => $Is_Unicode,
			'Is_Flash' => $Is_Flash
		);
		$curl = curl_init();

		curl_setopt_array($curl, array(
  		CURLOPT_URL => $url,
  		CURLOPT_RETURNTRANSFER => true,
  		CURLOPT_ENCODING => '',
  		CURLOPT_MAXREDIRS => 10,
  		CURLOPT_TIMEOUT => 0,
  		CURLOPT_FOLLOWLOCATION => true,
  		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  		CURLOPT_CUSTOMREQUEST => 'GET',
  		CURLOPT_POSTFIELDS => $content,
		));

		$response = curl_exec($curl);

		curl_close($curl);
		//echo $response;

	}

  public function send_notifikasi($invoice,$userid,$template)
    {
      $data_post = array(
        'invoice'     => $invoice,
        'userid'      => $userid,
        'template_id' => $template,
      );

      $curl = curl_init();

      curl_setopt_array($curl, array(
        CURLOPT_URL            => 'https://apipentest.elephate.co.id/dev/main/notifikasi/send_notif',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING       => '',
        CURLOPT_MAXREDIRS      => 10,
        CURLOPT_TIMEOUT        => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST  => 'POST',
        CURLOPT_POSTFIELDS     => $data_post,
        CURLOPT_HTTPHEADER     => array(
          'Token-Access: 7405d371f5cd524297e0ba7785d6ba30',
          'Cookie: ci_session=4qh7r9rkggnaot5mdrjca39v5rr1rni6'
        ),
      ));

      $response = curl_exec($curl);

      if ($response === false)  :
        $response = curl_error($curl);
      endif;
      curl_close($curl);
      // echo $response;


      $res = json_decode($response);
      return $res;
    }

    public function cek_stok_produk_paket($id_paket)
    {
      $curl = curl_init();

      // $id_paket = "602b92f16b11a";

      curl_setopt_array($curl, array(
        CURLOPT_URL            => 'https://apipentest.elephate.co.id/dev/main/product/stok_paket?id_paket='.$id_paket,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING       => '',
        CURLOPT_MAXREDIRS      => 10,
        CURLOPT_TIMEOUT        => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST  => 'GET',
        CURLOPT_HTTPHEADER => array(
          'Token-Access: 7405d371f5cd524297e0ba7785d6ba30',
          'Cookie: ci_session=3t7voj09em80s5kn2rg9q3fnf79243tn'
        ),
      ));

      $response = curl_exec($curl);

      $data = json_decode($response);
      curl_close($curl);
      if ($data == null) {
        return 0;
      }else{
        return @$data->stok;
      }
    }

        function get_data_bonus($periode, $userid)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://apipentest.elephate.co.id/dev/main/member/icentives?userid='.$userid.'&periode='.$periode,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_POSTFIELDS => [],
          CURLOPT_HTTPHEADER => array(
            'Token-Access: 7405d371f5cd524297e0ba7785d6ba30',
            'Cookie: ci_session=q99kv1c6ri17vi6b9t14vksjqr58fbee'
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return json_decode($response);
    }


    function arus_care_point($periode, $userid)
    {
        $curl    = curl_init();

        $tgl     = $periode.'-1';
        $periode = date("m/d/Y", strtotime($tgl));

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://apipentest.elephate.co.id/dev/incentive/care_point?userid='.$userid.'&periode='.$periode,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'Token-Access: 7405d371f5cd524297e0ba7785d6ba30',
            'Cookie: ci_session=d5eov2j2b83f6fgedkbidndeg3akb8fn'
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        return json_decode($response);
    }


}
