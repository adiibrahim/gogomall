<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class JettExpress {

    public static $isProduction = true;

    public static $client_key;

    const PRODUCTION_URL = 'https://api.jetexpress.co.id/v2';
    const SANDBOX_URL = 'https://api.sandbox.jetexpress.co.id/v2';

    public function config($params)
    {
        JettExpress::$client_key = $params['client_key'];
        JettExpress::$isProduction = $params['production'];
    }

        public static function getBaseUrl()
    {
        return JettExpress::$isProduction ?
            JettExpress::PRODUCTION_URL : JettExpress::SANDBOX_URL;
    }

        public static function post($url, $client_key, $params)
    {
        return self::remoteCall($url, $client_key, $params);
    }

        public static function remoteCall($url, $client_key, $params)
    {   
            
        $content = json_encode($params);

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST"); 
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array(
            "Content-type: application/json",
            "clientkey: ".$client_key,
        ));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

        $json_response = curl_exec($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        return json_decode($json_response);

    }


  public static function cekOngkir($params)
  {
    
    $result = JettExpress::post(
        JettExpress::getBaseUrl() . '/pricings',
        JettExpress::$client_key,
        $params);

    return $result;
  }

}