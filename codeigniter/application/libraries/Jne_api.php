<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jne_api {

    // public static $isProduction = true;

    public static $api_key;
    protected $url = "https://apiv2.jne.co.id";

    public function config($params)
    {
        // Jne_api::$client_key = $params['api_key'];
        // JettExpress::$isProduction = $params['production'];
    }

    //     public static function getBaseUrl()
    // {
    //     return JettExpress::$isProduction ?
    //         JettExpress::PRODUCTION_URL : JettExpress::SANDBOX_URL;
    // }

        public static function post($data)
    {
        return self::remoteCall($data);
    }

    public static function remoteCall($data)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $data['url']."/tracing/api/pricedev",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_PORT => "10102",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "username=TESTAPI&api_key=25c898a9faea1a100859ecd9ef674548&".$data['bodyfields'],
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);

        if ($response == false) {
            throw new Exception(curl_error($curl), curl_errno($curl));
        }

        curl_close($curl);
        return $response;
    }


    public static function cekOngkir($data)
    {
        $result = Jne_api::post($data);

        return $result;
    }

}