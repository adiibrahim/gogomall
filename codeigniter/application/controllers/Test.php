<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Test extends MY_Controller
{

    function __construct()
    {
        parent:: __construct();
    }

    public function index()
    {
        $sales = $this->global_model->get_global('sales')->result_array();

        $data = [
            'sales' => $sales,
        ];

        $this->load->view('content/test',$data);
    }

    public function data_customer()
    {
        $customer = $this->global_model->get_global('customer')->result();

        $data = [
            'customer' => $customer,
        ];

        $this->load->view('content/data_customer',$data);
    }

    public function post_customer()
    {
        $nama       = $this->input->post('nama');
        $notelp     = $this->input->post('notelp');
        $id_sales     = $this->input->post('id_sales');

        $data = [
            'nama'       => $nama,
            'no_telpon' => $notelp,
            'id_sales' => $id_sales,
        ];

        $test = $this->global_model->add_global('customer', $data);

        if($test):
            echo json_encode(['status' => 0]);
        else:
            echo json_encode(['status' => 1]);
        endif;
    }

    public function data_sales()
    {
        $sales = $this->global_model->get_global('sales')->result();

        $data = [
            'sales' => $sales,
        ];

        $this->load->view('content/data_sales',$data);
    }

    public function post_sales()
    {
        $nama       = $this->input->post('nama');

        $data = [
            'nama'       => $nama,
        ];

        $test = $this->global_model->add_global('sales', $data);

        if($test):
            echo json_encode(['status' => 0]);
        else:
            echo json_encode(['status' => 1]);
        endif;
    }

    public function select_data()
    {
        $sales = $this->global_model->get_global('sales')->result_array();

        $data = [
            'sales' => $sales,
        ];

        $this->load->view('content/select_data',$data);
    }

    public function view_list_data()
    {
        $listdata = $this->global_model->list_data()->result();

        $data = [
            'listdata' => $listdata,
        ];

        $this->load->view('content/data_report',$data);
    }

    public function update()
    {
        $id         = $this->input->post('id');
        $nama       = $this->input->post('nama');
        $tgl_lahir  = $this->input->post('tgl_lahir');
        $gaji       = $this->input->post('gaji');
        $status     = $this->input->post('status');

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://127.0.0.1:8001/api/karyawan/'.$id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => array('nama' => $nama,'tgl_lahir' => $tgl_lahir,'gaji' => $gaji,'status' => $status, '_method' => 'PUT'),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        echo $response;
    }

    public function edit()
    {
        $id = $this->input->post('param');

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://127.0.0.1:8001/api/karyawan/'.$id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_POSTFIELDS => array('nama' => 'Adi Ibrahims','tgl_lahir' => '26-08-1991','gaji' => '2000000','status' => '0','_method' => 'PUT'),
        ));

        $response = curl_exec($curl);

        var_dump($response);

        curl_close($curl);
        $response = json_decode($response);
        echo $response;
		// echo json_encode(array("data"=>$response));        


    }

    public function delete()
    {
        $id = $this->input->post('param');

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'http://127.0.0.1:8001/api/karyawan/'.$id,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'DELETE',
        CURLOPT_POSTFIELDS => array('nama' => 'Adi Ibrahims','tgl_lahir' => '26-08-1991','gaji' => '2000000','status' => '0','_method' => 'PUT'),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        // echo $response;

        // var_dump($id);
        echo "oke";
    }

    
}
