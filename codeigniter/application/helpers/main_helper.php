<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');



function capitalize($str)

{

    return ucwords($str);

}



function uppercase($str)

{

    return strtoupper($str);

}



function lowercase($str)

{

    return strtolower ($str);

}



function replace_underscore($var)

{

    return str_replace("_"," ",$var);

}

function replace_br($var)

{

    return str_replace("<br>"," ",$var);

}


function replace_strip($var)

{

    return str_replace("-","",$var);

}

function replace_strip_to_slash($var)

{

    return str_replace("-","/",$var);

}

function replace_strip_to_space($var)

{

    return str_replace("-"," ",$var);

}



function replace_space_to_strip($var)

{

    return str_replace(" ","-",$var);

}


function replace_space($var)

{

    return str_replace(" ","",$var);

}



function replace_coma($var)

{

    return str_replace(",","",$var);

}



function replace_titik($var)

{

    return str_replace(".","",$var);

}



function numeric_indo($var)

{

    return number_format($var, '0', '', '.');

}

function numeric_indo2($var)

{

    return number_format($var, '2', '.', '');

}



function numeric_uang($var)

{

    return number_format($var,2, ',', '.');

}



function numeric_uang2($var)

{

    return number_format($var,2, ',', '.');

}



function kg($var)

{

    return ceil(number_format($var / 1000, 1));;

}



function indo_date($date)

{

    return date('d-m-Y',strtotime($date));

}



function indo_date_time($date)

{

    return date('d-m-Y H:i:s',strtotime($date));

}



function inggris_date($date)

{

    return date('Y-m-d',strtotime($date));

}



function inggris_date_time($date)

{

    return date('Y-m-d H:i:s',strtotime($date));

}

function short_ukuran_file( $n, $precision = 1 ) {
    if ($n < 1024) {
        // 0 - 900
        $n_format = number_format($n, $precision);
        $suffix = ' KB ';
    } else if ($n < 1048576) {
        // 0.9k-850k
        $n_format = number_format($n / 1000, $precision);
        $suffix = ' MB ';
    } else if ($n < 1073741824) {
        // 0.9m-850m
        $n_format = number_format($n / 1000000, $precision);
        $suffix = ' GB ';
    } else if ($n < 1099511627776) {
        // 0.9b-850b
        $n_format = number_format($n / 1000000000, $precision);
        $suffix = 'TB';
    }
 
    if ( $precision > 0 ) {
        $dotzero = '.' . str_repeat( '0', $precision );
        $n_format = str_replace( $dotzero, '', $n_format );
    }
 
    return $n_format . $suffix;
}

function number_format_short( $n, $precision = 1 ) {
    if ($n < 900) {
        // 0 - 900
        $n_format = number_format($n, $precision);
        $suffix = '';
    } else if ($n < 900000) {
        // 0.9k-850k
        $n_format = number_format($n / 1000, $precision);
        $suffix = 'K';
    } else if ($n < 900000000) {
        // 0.9m-850m
        $n_format = number_format($n / 1000000, $precision);
        $suffix = 'M';
    } else if ($n < 900000000000) {
        // 0.9b-850b
        $n_format = number_format($n / 1000000000, $precision);
        $suffix = 'B';
    } else {
        // 0.9t+
        $n_format = number_format($n / 1000000000000, $precision);
        $suffix = 'T';
    }
 
    if ( $precision > 0 ) {
        $dotzero = '.' . str_repeat( '0', $precision );
        $n_format = str_replace( $dotzero, '', $n_format );
    }
 
    return $n_format . $suffix;
}

function tanggal_indo($tanggal, $cetak_hari = false)
{
    $hari = array ( 1 =>    'Senin',
                'Selasa',
                'Rabu',
                'Kamis',
                'Jumat',
                'Sabtu',
                'Minggu'
            );
            
    $bulan = array (1 =>   'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );
    $split    = explode('-', $tanggal);
    $tgl_indo = $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
    
    if ($cetak_hari) {
        $num = date('N', strtotime($tanggal));
        return $hari[$num] . ', ' . $tgl_indo;
    }
    return $tgl_indo;
}

function tanggal_indo2($tanggal, $cetak_hari = false)
{
    $hari = array ( 1 =>    'Senin',
                'Selasa',
                'Rabu',
                'Kamis',
                'Jumat',
                'Sabtu',
                'Minggu'
            );
    
    $tgl_indo =  date('d-m-Y');

    if ($cetak_hari) {
        $num = date('N', strtotime($tanggal));
        return $hari[$num] . ', ' . $tgl_indo;
    }
    return $tgl_indo;
}

function time_to_dot($date)
{
    return date('H.i',strtotime($date));
}

function time_AM_PM($date)
{
    return date('H.i A',strtotime($date));
}

function bulan_indo($tanggal)
{
    $bulan = array (1 =>   'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            );
    $split    = explode('-', $tanggal);
    $tgl_indo = $bulan[ (int)$split[1] ] . ' ' . $split[0];
    return $tgl_indo;
}

function humanTiming ($time) {
    $time = time() - $time; // to get the time since that moment
    $time = ($time<1)? 1 : $time;
    $tokens = array (
        31536000 => 'tahun',
        2592000 => 'bulan',
        604800 => 'minggu',
        86400 => 'hari',
        3600 => 'jam',
        60 => 'menit',
        1 => 'detik'
    );

    foreach ($tokens as $unit => $text) {
    if ($time < $unit) continue;
        $numberOfUnits = floor($time / $unit);
        return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'':'');
    }

}


function like_match($pattern, $subject)
{
    $pattern = str_replace('%', '.*', preg_quote($pattern, '/'));
    return (bool) preg_match("/^{$pattern}$/i", $subject);
}

function hp($nohp)
{
    // kadang ada penulisan no hp 0811 239 345
    $nohp = str_replace(" ","",$nohp);
    // kadang ada penulisan no hp (0274) 778787
    $nohp = str_replace("(","",$nohp);
    // kadang ada penulisan no hp (0274) 778787
    $nohp = str_replace(")","",$nohp);
    // kadang ada penulisan no hp 0811.239.345
    $nohp = str_replace(".","",$nohp);
    // kadang ada penulisan no hp 0811-239-345
    $nohp = str_replace("-","",$nohp);
 
    // cek apakah no hp mengandung karakter + dan 0-9
    if(!preg_match('/[^+0-9]/',trim($nohp))){
        // cek apakah no hp karakter 1-3 adalah +62
        if(substr(trim($nohp), 0, 3)=='+62'){
            $hp = '62'.substr(trim($nohp), 3);
        }
        // cek apakah no hp karakter 2 adalah 62
        elseif(substr(trim($nohp), 0, 2)=='62'){
            $hp = trim($nohp);
        }
        // cek apakah no hp karakter 1 adalah 0
        elseif(substr(trim($nohp), 0, 1)=='0'){
            $hp = '62'.substr(trim($nohp), 1);
        }
    }
    return $hp;
}