<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
    <div class="container mb-5">
        <div class="card mb-5 mt-5">
            <div class="card-body">
                <h6 class="text-center mt-2 mb-2">Form Sales</h6>
                <form action="method" action="#" id="formSales">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group mb-3">
                                <label for="">Nama</label>
                                <input type="text" id="nama" name="nama" class="form-control" placeholder="masukan nama">
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group mb-3">
                                <button id="btnSimpanSales" type="button" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card">
            <h6 class="pt-4 mb-2">Data Sales</h6>
            <div id="viewdata_sales"></div>
        </div>

    </div>


    <div class="container">
        <div class="card mb-5 mt-5">
            <div class="card-body">
                <h6 class="text-center mt-2 mb-2">Form Customer</h6>
                <form action="method" action="#" id="formCustomer">
                    <div class="row">
                    <input type="hidden" id="id" name="id" class="form-control"><input type="hidden" id="id" name="id" class="form-control">
                        <div class="col-lg-12">
                            <div class="form-group mb-3">
                                <label for="">Sales</label>
                                <div id="select_data"></div>
                            </div>
                        </div>    
                        <div class="col-lg-12">
                            <div class="form-group mb-3">
                                <label for="">Nama</label>
                                <input type="text" id="nama" name="nama" class="form-control" placeholder="masukan nama">
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group mb-3">
                                <label for="">No_telpon</label>
                                <input type="text" id="notelp" name="notelp" class="form-control" placeholder="masukan nomor telpon">
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group mb-3">
                                <button id="btnSimpan" type="button" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card">
            <h6 class="pt-4 mb-2">Data Customer</h6>
            <div id="viewdata_customer"></div>
        </div>


        <div class="card">
            <h6 class="pt-4 mb-2">List Data</h6>
            <div id="view_list_data"></div>
        </div>

    </div>
</body>

<script>
    $(document).ready(function(){
        viewdata_customer();
        viewdata_sales();
        select_data();
        view_list_data();
    });

    $('#btnSimpan').on('click', function() {
        var formData = new FormData($("#formCustomer").get(0));
        $.ajax({
            url: "<?= base_url('test/post_customer');?>",
            type: "POST",
            cache: false,
            processData: false,
            contentType: false,
            data: formData,
            dataType:'json',
            success: function(result){
                console.log(result);
                viewdata_customer();
                select_data();
                view_list_data();
            }
        });
    })

    function viewdata_customer() {
        $.post('<?= base_url("test/data_customer");?>', function(response){
            $('#viewdata_customer').html(response);

        });
    }

    $('#btnSimpanSales').on('click', function() {
        // e.preventDefault();
        var formData = new FormData($("#formSales").get(0));
        $.ajax({
            url: "<?= base_url('test/post_sales');?>",
            type: "POST",
            cache: false,
            processData: false,
            contentType: false,
            data: formData,
            dataType:'json',
            success: function(result){
                console.log(result);
                viewdata_sales();
                select_data();
                view_list_data();
            }
        });
    })

    function viewdata_sales() {
        $.post('<?= base_url("test/data_sales");?>', function(response){
            $('#viewdata_sales').html(response);

        });
    }

    function select_data() {
        $.post('<?= base_url("test/select_data");?>', function(response){
            $('#select_data').html(response);

        });
    }

    function view_list_data() {
        $.post('<?= base_url("test/view_list_data");?>', function(response){
            $('#view_list_data').html(response);
        });
    }
</script>
</html>