<?php 

$lang['form_validation_required']              = '{field} tidak boleh kosong.';
$lang['form_validation_isset']                 = '{field} field harus diisi.';
$lang['form_validation_valid_email']           = '{field} tidak valid.';
$lang['form_validation_valid_emails']          = '{field} tidak valid.';
$lang['form_validation_valid_url']             = '{field} harus berisi URL yang valid.';
$lang['form_validation_valid_ip']              = '{field} harus berisi IP yang valid.';
$lang['form_validation_valid_base64']          = '{field} harus berisi string Base64 yang valid.';
$lang['form_validation_min_length']            = '{field} minimal {param} karakter.';
$lang['form_validation_max_length']            = '{field} tidak boleh melebihi {param} karakter.';
$lang['form_validation_exact_length']          = '{field} panjangnya harus tepat {param} karakter.';
$lang['form_validation_alpha']                 = '{field} hanya boleh berisi karakter alfabet.';
$lang['form_validation_alpha_numeric']         = '{field} hanya boleh berisi karakter alfanumerik.';
$lang['form_validation_alpha_numeric_spaces']  = '{field} hanya boleh berisi karakter alfanumerik dan spasi.';
$lang['form_validation_alpha_dash']            = '{field} hanya diperbolehkan huruf';
$lang['form_validation_numeric']               = '{field} harus hanya berisi angka.';
$lang['form_validation_is_numeric']            = '{field} hanya boleh berisi karakter numerik.';
$lang['form_validation_integer']               = '{field} harus berisi bilangan bulat.';
$lang['form_validation_regex_match']           = '{field} tidak dalam format yang benar.';
$lang['form_validation_matches']               = '{field} tidak cocok dengan {param}.';
$lang['form_validation_differs']               = '{field} harus berbeda dari bidang {param}.';
$lang['form_validation_is_unique']             = '{field} sudah digunakan!.';
$lang['form_validation_is_natural']            = '{field} hanya boleh berisi angka.';
$lang['form_validation_is_natural_no_zero']    = '{field} hanya boleh berisi angka dan harus lebih besar dari nol.';
$lang['form_validation_decimal']               = '{field} harus berisi angka desimal.';
$lang['form_validation_less_than']             = '{field} harus berisi angka kurang dari {param}.';
$lang['form_validation_less_than_equal_to']    = '{field} harus berisi angka yang kurang dari atau sama dengan {param}.';
$lang['form_validation_greater_than']          = '{field} harus berisi angka yang lebih besar dari {param}.';
$lang['form_validation_greater_than_equal_to'] = '{field} harus berisi angka yang lebih besar atau sama dengan {param}.';
$lang['form_validation_error_message_not_set'] = 'Tidak dapat mengakses pesan kesalahan yang sesuai dengan nama Anda {field}.';
$lang['form_validation_in_list']               = '{field} harus salah satu dari: {param}.';


 ?>